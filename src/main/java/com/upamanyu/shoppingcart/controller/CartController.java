package com.upamanyu.shoppingcart.controller;

import com.upamanyu.shoppingcart.dto.CartDTO;
import com.upamanyu.shoppingcart.dto.CartItemDTO;

import com.upamanyu.shoppingcart.entity.Product;
import com.upamanyu.shoppingcart.service.CartService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {

    private CartService cartService;

    CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @GetMapping("/allProducts")
    public List<Product> getProducts() {
        // return a list of products from stock
        return cartService.getAllProducts();
    }

    // get all items for a cart

    @GetMapping("/get-cart-products/{cartId}")
    public CartDTO getCartProducts(@PathVariable(name="cartId") long cartId) {
        // return a list of products from stock
        return cartService.getCartDetails(cartId);
    }

    // add cart items
    @PatchMapping("{cartId}/add-to-cart/{productId}")
    public CartDTO increaseProductQuantityCart(@PathVariable(name="cartId") long cartId
    , @PathVariable(name="productId") long productId) {
        // increase cart quantity
        return cartService.increaseProductInCart(cartId,productId);
    }

    @PatchMapping("{cartId}/decrease-from-cart/{productId}")
    public CartDTO decreaseProductQuantityCart(@PathVariable(name="cartId") long cartId
    , @PathVariable(name="productId") long productId) {
        // decrease cart quantity
        return cartService.decreaseProductInCart(cartId,productId);
    }

    @PostMapping("{cartId}/add-new-to-cart/{productId}")
    public CartItemDTO addNewProductCart(@PathVariable(name="cartId") long cartId
            , @PathVariable(name="productId") long productId) {
        // add new item to cart
        // create new cart-item row
        return cartService.addProductToCart(cartId,productId);

    }

    @DeleteMapping("{cartId}/remove-from-cart/{productId}")
    public ResponseEntity<String> removeProductCart(@PathVariable(name="cartId") long cartId
            , @PathVariable(name="productId") long productId) {
        // remove from cart
        cartService.removeProductFromCart(cartId,productId);
        return new ResponseEntity<>(HttpStatus.OK);
    }




}
