package com.upamanyu.shoppingcart.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CartDTO {

    public String name;

    public String email;


    public List<CartItemDTO> cartItems;

    public CartDTO(String name, String email) {
        this.name = name;
        this.email = email;
    }

}
