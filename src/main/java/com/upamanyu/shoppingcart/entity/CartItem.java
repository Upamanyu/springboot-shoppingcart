package com.upamanyu.shoppingcart.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="cart_item")
@Data
@NoArgsConstructor
public class CartItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    @JoinColumn(name="product_id")
    private Product product;

    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.DETACH})
    @JoinColumn(name="cart_id")
    private Cart cart;

    @Column(name="amount")
    private int amount;

    public CartItem(Product product, Cart cart, int amount) {
        this.product = product;
        this.cart = cart;
        this.amount = amount;
    }
}
