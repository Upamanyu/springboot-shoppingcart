package com.upamanyu.shoppingcart.exception;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({CartNotFoundException.class})
    public ResponseEntity<CartNotFoundException> cartException(CartNotFoundException e) {
        return new ResponseEntity<CartNotFoundException>(e,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({CartItemNotFoundException.class})
    public ResponseEntity<CartItemNotFoundException> cartItemException(CartItemNotFoundException e) {
        return new ResponseEntity<CartItemNotFoundException>(e,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ProductNotFoundException.class})
    public ResponseEntity<ProductNotFoundException> productNotFoundException(ProductNotFoundException e) {
        return new ResponseEntity<ProductNotFoundException>(e,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({OutOfStockException.class})
    public ResponseEntity<OutOfStockException> productNotFoundException(OutOfStockException e) {
        return new ResponseEntity<OutOfStockException>(e,HttpStatus.BAD_REQUEST);
    }


}
