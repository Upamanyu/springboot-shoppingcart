package com.upamanyu.shoppingcart.exception;

import com.upamanyu.shoppingcart.entity.Product;

public class ProductNotFoundException extends RuntimeException {

    public ProductNotFoundException(String message) {
        super(message);
    }

}
