package com.upamanyu.shoppingcart.repository;

import com.upamanyu.shoppingcart.entity.Cart;
import com.upamanyu.shoppingcart.entity.CartItem;
import com.upamanyu.shoppingcart.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CartItemRepository extends JpaRepository<CartItem,Long> {

    Optional<CartItem>  findByCartAndProduct(Cart cart, Product product);

    void deleteByCartAndProduct(Cart currentCart, Product currentProduct);
}
