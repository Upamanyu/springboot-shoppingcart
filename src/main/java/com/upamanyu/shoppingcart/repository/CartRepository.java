package com.upamanyu.shoppingcart.repository;

import com.upamanyu.shoppingcart.entity.Cart;
import com.upamanyu.shoppingcart.entity.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CartRepository extends JpaRepository<Cart,Long> {
    @Query("SELECT c FROM Cart c WHERE c.user.name = :name AND c.user.email = :email")
    Optional<Cart> findByNameAndEmail(@Param("name") String name,@Param("email") String email);

    // jpa repository methods to find related entities
    @Query("SELECT i FROM Cart c JOIN FETCH CartItem i ON i.cart = :cart")
    public List<CartItem> findCartItems(@Param("cart") Cart theCart);


}
