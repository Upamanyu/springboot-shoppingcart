package com.upamanyu.shoppingcart.repository;

import com.upamanyu.shoppingcart.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Long> {
}
