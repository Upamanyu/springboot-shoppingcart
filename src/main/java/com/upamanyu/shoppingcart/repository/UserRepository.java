package com.upamanyu.shoppingcart.repository;

import com.upamanyu.shoppingcart.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
}
