package com.upamanyu.shoppingcart.service;

import com.upamanyu.shoppingcart.dto.CartDTO;
import com.upamanyu.shoppingcart.dto.CartItemDTO;
import com.upamanyu.shoppingcart.entity.Product;

import java.util.List;

public interface CartService {
    List<Product> getAllProducts();

    public CartDTO getCartDetails(long cartId);

    CartDTO increaseProductInCart(long cartId, long productId);

    CartDTO decreaseProductInCart(long cartId, long productId);

    void removeProductFromCart(long cartId, long productId);

    CartItemDTO addProductToCart(long cartId, long productId);

}
