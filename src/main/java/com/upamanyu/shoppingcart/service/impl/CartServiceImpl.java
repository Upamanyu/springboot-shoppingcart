package com.upamanyu.shoppingcart.service.impl;

import com.upamanyu.shoppingcart.dto.CartDTO;
import com.upamanyu.shoppingcart.dto.CartItemDTO;
import com.upamanyu.shoppingcart.dto.UserDTO;
import com.upamanyu.shoppingcart.entity.Cart;
import com.upamanyu.shoppingcart.entity.CartItem;
import com.upamanyu.shoppingcart.entity.Product;
import com.upamanyu.shoppingcart.exception.CartItemNotFoundException;
import com.upamanyu.shoppingcart.exception.CartNotFoundException;
import com.upamanyu.shoppingcart.exception.OutOfStockException;
import com.upamanyu.shoppingcart.exception.ProductNotFoundException;
import com.upamanyu.shoppingcart.repository.CartItemRepository;
import com.upamanyu.shoppingcart.repository.CartRepository;
import com.upamanyu.shoppingcart.repository.ProductRepository;
import com.upamanyu.shoppingcart.repository.UserRepository;
import com.upamanyu.shoppingcart.service.CartService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Service
public class CartServiceImpl implements CartService {

    private Logger logger;

    private UserRepository userRepository;
    private CartRepository cartRepository;
    private ProductRepository productRepository;
    private CartItemRepository cartItemRepository;

    public CartServiceImpl(UserRepository userRepository, CartRepository cartRepository,
                           ProductRepository productRepository, CartItemRepository cartItemRepository) {

        this.userRepository = userRepository;
        this.cartRepository = cartRepository;
        this.cartItemRepository = cartItemRepository;
        this.productRepository = productRepository;

    }

    public List<Product> getAllProducts() {
        List<Long> ids =  List.of(1L,2L,3L);
        return productRepository.findAllById(ids);
        // return productRepository.findAll();
    }

    @Override
    @Transactional
    public CartDTO increaseProductInCart(long cartId, long productId) {
        // fetch cart and product
        Optional<Cart> currentCartOpt = cartRepository.findById(cartId);
        Optional<Product> currentProductOpt = productRepository.findById(productId);
        if(currentCartOpt.isEmpty()) {
            throw new CartNotFoundException("cart not found");
        }
        if(currentProductOpt.isEmpty()) {
            throw new ProductNotFoundException("product not found");
        }
        Cart currentCart = currentCartOpt.get();
        Product currentProduct = currentProductOpt.get();
        Optional<CartItem> cartItemOpt =
                cartItemRepository.findByCartAndProduct(currentCart, currentProduct);
        if (cartItemOpt.isPresent()) {
            // update the existing database entry
            CartItem updatedCartItem = cartItemOpt.get();
            int productAvailableQty = currentProduct.getAvailableQuantity();
            int newQty = updatedCartItem.getAmount();
            if (1 <= productAvailableQty) {
                newQty += 1;
                productAvailableQty -= 1;
            }
            updatedCartItem.setAmount(newQty);
            currentProduct.setAvailableQuantity(productAvailableQty);
            productRepository.save(currentProduct);
            cartItemRepository.save(updatedCartItem);
        } else {
            // does not exist throw error
            throw new CartItemNotFoundException("Product is not available in cart!");
        }
        // return cart details
        return null;
    }
    @Override
    @Transactional
    public CartDTO decreaseProductInCart(long cartId, long productId) {
        // if quantity becomes 0 remove from CartItem table
        // fetch cart and product
        Optional<Cart> currentCartOpt = cartRepository.findById(cartId);
        Optional<Product> currentProductOpt = productRepository.findById(productId);
        if(currentCartOpt.isEmpty()) {
            throw new CartNotFoundException("cart not found");
        }
        if(currentProductOpt.isEmpty()) {
            throw new ProductNotFoundException("product not found");
        }
        Cart currentCart = currentCartOpt.get();
        Product currentProduct = currentProductOpt.get();
        Optional<CartItem> cartItemOpt =
                cartItemRepository.findByCartAndProduct(currentCart, currentProduct);
        if (cartItemOpt.isPresent()) {
            // update the existing database entry
            CartItem updatedCartItem = cartItemOpt.get();
            int productAvailableQty = currentProduct.getAvailableQuantity();
            int newQty = updatedCartItem.getAmount();
            if (newQty - 1 >= 0) {
                newQty -= 1;
                productAvailableQty += 1;
            }
            else {
                // throw error
                // not enough items!
            }
            updatedCartItem.setAmount(newQty);
            currentProduct.setAvailableQuantity(productAvailableQty);
            productRepository.save(currentProduct);
            cartItemRepository.save(updatedCartItem);
        } else {
            // does not exist throw error
            throw new CartItemNotFoundException("Product is not available in cart!");
        }

        return createCartDTO(currentCart);
    }

    @Override
    public void removeProductFromCart(long cartId, long productId) {
        Optional<Cart> currentCartOpt = cartRepository.findById(cartId);
        Optional<Product> currentProductOpt = productRepository.findById(productId);

        if(currentCartOpt.isEmpty()) {
            throw new CartNotFoundException("cart not found");
        }

        if(currentProductOpt.isEmpty()) {
            throw new ProductNotFoundException("product not found");
        }
        Cart currentCart = currentCartOpt.get();
        Product currentProduct = currentProductOpt.get();
        // get the cart item and its quantity
        // remove from cart
        Optional<CartItem> optionalCartItem = cartItemRepository.
                findByCartAndProduct(currentCart, currentProduct);
        if(optionalCartItem.isPresent()) {
            CartItem theCartItem = optionalCartItem.get();
            cartItemRepository.delete(theCartItem);
            // when you delete from cart add the items back into product stock
            int newQty  = currentProduct.getAvailableQuantity() + theCartItem.getAmount();
            currentProduct.setAvailableQuantity(newQty);
            productRepository.save(currentProduct);
        }
        else {
            throw new CartItemNotFoundException("cart item not found to be removed");
        }

    }

    @Override
    public CartItemDTO addProductToCart(long cartId, long productId) {
        Optional<Cart> currentCartOpt = cartRepository.findById(cartId);
        Optional<Product> currentProductOpt = productRepository.findById(productId);
        if(currentCartOpt.isEmpty()) {
            throw new CartNotFoundException("cart not found");
        }
        if(currentProductOpt.isEmpty()) {
            throw new ProductNotFoundException("product not found");
        }
        // add to cart
        Cart currentCart = currentCartOpt.get();
        Product currentProduct = currentProductOpt.get();
        Optional<CartItem> cartItemOpt =
                cartItemRepository.findByCartAndProduct(currentCart, currentProduct);

        if(currentProduct.getAvailableQuantity() == 0) {
            // can't be added to cart
            throw new OutOfStockException("The product is out of stock and can't be added!");
        }
        currentProduct.setAvailableQuantity(currentProduct.getAvailableQuantity()-1);
        CartItem newCartItem = cartItemOpt.orElseGet(()-> new CartItem(currentProduct,currentCart,1));

        cartItemRepository.save(newCartItem);
        productRepository.save(currentProduct);

        CartItemDTO cartItemDTO = new CartItemDTO(
                newCartItem.getProduct().getProductName(),
                newCartItem.getAmount(),
                newCartItem.getProduct().getPrice()
                );
        return cartItemDTO;
    }

    public CartDTO getCartDetails(UserDTO userDto) {

        Optional<Cart> cartOpt = cartRepository
                .findByNameAndEmail(userDto.getName(),userDto.getEmail());
        if(cartOpt.isEmpty()) {
            throw new CartNotFoundException("Cart not found. check user details");
        }
        // load cart items with cartItems
        Cart theCart = cartOpt.get();
        return createCartDTO(theCart);
    }

    public CartDTO getCartDetails(long cartId) {
        Optional<Cart> theCartOpt = cartRepository.findById(cartId);

        if(theCartOpt.isEmpty()) return null;

        Cart theCart = theCartOpt.get();

        return createCartDTO(theCart);

    }

    private CartDTO createCartDTO(Cart theCart) {
        CartDTO cartDTO = new CartDTO();
        cartDTO.setName(theCart.getUser().getName());
        cartDTO.setEmail(theCart.getUser().getEmail());

        // lazily load all cart items
        List<CartItem> theCartItems = cartRepository.findCartItems(theCart);
        List<CartItemDTO> cartItemDTOList = theCartItems.stream()
                        .map(cartItem -> {
                            String productName = cartItem.getProduct().getProductName();
                            int qty = cartItem.getAmount();
                            double price = qty*cartItem.getProduct().getPrice();
                            return new CartItemDTO(productName,qty,price);
                        }).toList();

        cartDTO.setCartItems(cartItemDTOList);
        return cartDTO;
    }

}
