insert into product (product_name,available_qty,price) values
('Macbook Pro Laptop',200,51999.99),
('IPhone 15',35,85000),
('Playstation 5',200,50999),
('Playstation 5 Pro',5,55000);


insert into users (name,email) values
('Upamanyu','u@gmail.com'),
('Subham','s@gmail.com'),
('Bishwayan','b@gmail.com'),
('KRISHNENDU SADHU','k@gmail.com');

insert into cart (user_id) values
(1),
(2),
(3),
(4);